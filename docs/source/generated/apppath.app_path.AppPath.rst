apppath.app\_path.AppPath
=========================

.. currentmodule:: apppath.app_path

.. autoclass:: AppPath
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~AppPath.__init__
      ~AppPath.app_version
      ~AppPath.clean
      ~AppPath.clean_site_config
      ~AppPath.clean_site_data
      ~AppPath.clean_user_cache
      ~AppPath.clean_user_config
      ~AppPath.clean_user_data
      ~AppPath.clean_user_log
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~AppPath.app_author
      ~AppPath.app_name
      ~AppPath.root_cache
      ~AppPath.root_config
      ~AppPath.root_log
      ~AppPath.root_long_tmp
      ~AppPath.root_run
      ~AppPath.root_state
      ~AppPath.root_tmp
      ~AppPath.site_cache
      ~AppPath.site_config
      ~AppPath.site_data
      ~AppPath.site_log
      ~AppPath.user_cache
      ~AppPath.user_config
      ~AppPath.user_data
      ~AppPath.user_log
      ~AppPath.user_state
   
   