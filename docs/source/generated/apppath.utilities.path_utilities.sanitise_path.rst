apppath.utilities.path\_utilities.sanitise\_path
================================================

.. currentmodule:: apppath.utilities.path_utilities

.. autofunction:: sanitise_path