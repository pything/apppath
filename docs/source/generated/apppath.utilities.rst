apppath.utilities
=================

.. automodule:: apppath.utilities

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom_autosummary/module.rst
   :recursive:

   apppath.utilities.path_utilities
   apppath.utilities.windows_path_utilities

